import os
import RPi.GPIO as gpio
import random
import time
from mutagen.mp3 import MP3
import picamera
# basic imports

len_audio = ""
 

# *****************************************
# ****************  Init  *****************
# *****************************************
pinsLed = [17,17]
pinEchoGet = 18 #ECHO
pinEchoSend = 4 #TRIG
trigerdistance = 150
spooktime=5
spookbreak=0
scaninterval=0.125
system = True # !!!
shortSamplesPath = "/home/pi/red/soundsamples"
soundSuffix = ".mp3"
camera = picamera.PiCamera()
camera.vflip = True





gpio.setmode(gpio.BCM)
for pin in pinsLed:
    gpio.setup(pin, gpio.OUT)
gpio.setup(pinEchoGet, gpio.IN)
gpio.setup(pinEchoSend, gpio.OUT)
# sets the gpio pins




def scan_break():
    time.sleep(scaninterval)
# func that waits out the time set in the init



    
    
    




    
# ich nehme mal die Schleife raus und werfe sie in die Main:
def distance_sens():
    gpio.output(pinEchoSend, True)
    time.sleep(0.0001)
    gpio.output(pinEchoSend, False)

    while gpio.input(pinEchoGet) == False:
        start = time.time()
    while gpio.input(pinEchoGet) == True:
        end = time.time()

    sig_time = end-start
    distance = sig_time / 0.000058
    print(distance)
    return distance
# this func uses the distance sensor and returns and prints the distance


    
# *****************************************
# ****************  Main  *****************
# *****************************************
while True:
    scan_break()
    firstDistance = distance_sens()
    if firstDistance <= trigerdistance:
        scan_break()
        # if the distance becomes smaller or stays the same go on.. (could be we have to rise the time)
        if distance_sens() <= firstDistance:
            camera.capture("{}.jpg".format(time.time()))
            time.sleep(spookbreak) # is currently 0 so ignore it
            
            # due to the playsound being in the background and py running we have to set the breaks in the lights functions
            # after lights is pased the audio will be killed and the lights shut
            



gpio.cleanup()


