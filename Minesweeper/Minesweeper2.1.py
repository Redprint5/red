import tkinter as tk
from tkinter import *
import random
from tkinter import messagebox
import time
import sys





system_frame_size = [0.72,0.72]



class Game:
    

        
    def __init__(self):
        # starting difictulty and cheats
        self.diffictulty = "easy"
        self.cheats = False
        # this activates cheats on status True

        # main
        root = tk.Tk()
        root.title("Minesweeper")


        # canvas is made for the frames to be placed in
        self.canvas = tk.Canvas(root,width= 400,height= "350",bg="#263D42")
        self.canvas.pack()
        self.main()
        root.mainloop()
        
    def main(self):
        # frame for mines and flags
        self.systemframe = tk.Frame(self.canvas, bg="black")
        self.systemframe.place(relwidth= system_frame_size[1], relheight= system_frame_size[0], relx= 0.15, rely=0.2)
        
        self.label = tk.Label(self.canvas, height="1", width="1", fg="white")
        self.mine_list = []
        self.flag_list = []
        self.flag_dict = {}
        self.zero_dict = {}
        self.zero_list = []
        self.total_dict = {}

        # minecounters for each diffuctly 
        if self.diffictulty == "easy":
            self.coloumnlist = 8
            self.rowlist = 8
        if self.diffictulty == "medium":
            self.coloumnlist = 12
            self.rowlist = 12
        if self.diffictulty == "hard":
            self.coloumnlist = 16
            self.rowlist = 16

        # creates mines
        for coloumnall in range(self.coloumnlist):
            for rowall in range(self.rowlist):
                # 1/3 chance that a mine will be placed
                if random.randint(1,4) == 3:
                    self.mine_list.append("{},{}".format(rowall,coloumnall))
                else:
                    self.flag_list.append("{},{}".format(rowall,coloumnall))
        # this is a sorter that removes mines next to each other to make the game more playable(to many mines made)            
        nd_mine = []            
        for i in self.mine_list:
            b,x = i.split(",")
            rowcheck = int(b)
            columncheck = int(x)
            for i in self.mine_list:
                y,k = i.split(",")
                if rowcheck == int(y)-1 and int(y)+1:
                    if columncheck == int(k)-1 or int(k)+1: 
                        nd_mine.append("{},{}".format(rowcheck,columncheck))
        self.final_mine = list(set(nd_mine)) 
        self.wincounter = self.final_mine   
##        print("final",self.final_mine)                
        
        # this assigns the functions to the buttons
        self.minecounterlist = []        
        for i in self.final_mine:
            row,column = i.split(",")
            self.mine = tk.Button(self.systemframe, bg="white",fg="white")
            self.mine.config(height ="1", width="1",command=lambda a=b,j=x,l=self.mine, c=i: self.action_leftright(a,j,l, True,c))
            h=self.mine
            self.mine.bind("<Button-3>", lambda a=row,j=column,l=self.mine,c=i: self.action_leftright(a,j,l, False,c))
            self.mine.grid(row=row, column = column,sticky = W)
            self.minecounterlist.append(str(row+ ","+ column))
#        print(self.minecounterlist)
        ##############
        #buttoncreate#
        ##############
        # this counts the mines for the flags
        x = 0
        for coloumn in range(self.coloumnlist):
            for row in range(self.rowlist):
                string = "{},{}".format(row,coloumn)
                if string in self.minecounterlist:
##                    print("true")
                    x += 1
##                    print(x)


                    
                     
        # flag making with rows and columns provided by self.xlist
        # flagcheck checks for mines around flag placed = flagpoints
        for coloumn in range(self.coloumnlist):
            for row in range(self.rowlist):
                mine ="{},{}".format(row,coloumn)
                if mine in self.minecounterlist:
                    pass
                else:
                    flagpoints = self.flagcheck(row,coloumn)
                    self.flag = tk.Button(self.systemframe, bg="white",fg="white")
                    position = "{},{}".format(row,coloumn)
                    self.total_dict[position] = self.flag
                    if flagpoints != 0:
                        self.flag_dict[position] = self.flag
                        self.zero_list.append(position)
                    else:
                        self.zero_dict[position] = self.flag

                    self.flag.config(height ="1", width="1",command=lambda i=row,j=coloumn,l=self.flag,g=flagpoints: self.action_leftright_flag(i,j,l, True,g))
                    if self.cheats == True:
                        self.flag.config(text="{}".format(flagpoints))
                    # this accivates cheats
                    h=self.flag
                    self.flag.bind("<Button-3>", lambda i=row,j=coloumn,l=self.flag, g=flagpoints: self.action_leftright_flag(i,j,l, False,g))
                    self.flag.grid(row=row, column = coloumn,sticky = W)







#         for i in self.zero_dict:
# #            print(self.zero_dict[i])
#             self.zero_dict[i].config(bg="green")


##                else:
##                    x = tk.Button(systemframe, bg="black",fg="white")
##                    x.config(height ="1", width="1")
##                    x.grid(row=i, column = y)
        
        #############
        # Innit v.2 #
        #############
        
        # this is the continuation of the INNIT of the class
        # this is for reseting the stage
        self.systembuttonsframe = tk.Frame(self.canvas, bg="blue")
        self.systembuttonsframe.place(relx="0.0",rely="0.0")
        self.resetbutton = tk.Button(self.systembuttonsframe, bg="blue",fg="white",command=self.reset, text="reset")
        self.resetbutton.grid(row="0",column="0")



        # Widgets for the dificulty
        self.diffuculty_frame = tk.Frame(self.canvas, bg="black", width="60",height="50")
        self.diffuculty_frame.place(relx="1",rely="0.0", anchor="ne")

        self.diffuculty_label = tk.Label(self.diffuculty_frame, bg="white", text="difficulty")
        self.diffuculty_label.place(relx="0.0",rely="0.0")

        self.diffictulty_easy = tk.Button(self.diffuculty_frame, height ="1", width="1", bg="green",fg = "white", command=self.change_dificutly_easy)
        self.diffictulty_easy.place(relx="0.0",rely="0.3")

        self.diffictulty_medium = tk.Button(self.diffuculty_frame, height ="1", width="1", bg="orange",fg = "white", command=self.change_dificutly_medium)
        self.diffictulty_medium.place(relx="0.2",rely="0.3")

        self.diffictulty_hard = tk.Button(self.diffuculty_frame, height ="1", width="1", bg="red",fg = "white", command=self.change_dificutly_hard)
        self.diffictulty_hard.place(relx="0.4",rely="0.3")




    # this is a check with the inputs if a mine is around it
    # prints are for comparison - disabled
    def flagcheck(self, input_row, input_column):            
        x = 0 
        for mine in self.final_mine:
             b,y = mine.split(",")
             row = b
             column = y
             for i in range(-1,2,1):
                 row_mod = int(row) - int(i)
                 for y in range(-1,2,1):
                     column_mod = int(column) - int(y)
                     if row_mod == input_row:
                        if column_mod == input_column:
##                            print("comparison {} and {}row".format(row_mod,input_row))
##                            print("comparison {} and {}column".format(column_mod,input_column))
                            x+=1
        return x                   
    
    # resets the mainfunction and restores everything
    def reset(self):
        self.systemframe.destroy()
        self.main()



    def change_dificutly_easy(self):
        self.canvas.config(width= 400,height= "350")
        self.diffictulty = "easy"
        self.reset()

    def change_dificutly_medium(self):
        self.canvas.config(width= 600,height= "525")
        self.diffictulty = "medium"
        self.reset()


    def change_dificutly_hard(self):
        self.canvas.config(width= 800,height= "700")
        self.diffictulty = "hard"
        self.reset()

    
    # this is funktions for right and leftclick for the flags
    def action_leftright_flag(self,minerow,minecolumn,mine_obj,State,points):
##        print("baum",minerow,minecolumn)
##        print(mine_obj)
##        z.config(state="disabled",text="2")
##        self.label.grid(row=x, column=y)
        if State == False:        
           #mine_obj.config(bg="green")
           pass
        if State == True:
            this_pos = self.str_mkr(minerow, minecolumn)
            mine_obj.config(text="{}".format(points),bg="blue")
           # this makes a string for the position witrh the str mkr func




           # this is start for the 0 function

            zero_nexttoeach = []
            if points == 0:
                position = this_pos
                used_flags = []
                used_flags.append(position)
                old_pos = zero_nexttoeach
                b,x = this_pos.split(",")

                while True:
                    # this checks a grid of 3x3 and the starting point is in the middle
                    # this checks the fields for other 0
                    zero_nexttoeach.append(position)
                    b,x = position.split(",")
                    old_list = zero_nexttoeach
                    for i in set(zero_nexttoeach):
                        row,column = i.split(",")
                        for row_mod in range(-1,2,1):
                            final_row = int(b)+int(row_mod)
                            for column_mod in range(-1,2,1):
                                    finalcolumn = int(x)+int(column_mod)
                                    zero_pos = self.str_mkr(final_row,finalcolumn)

                                    if int(column_mod)+int(final_row) != 0:
                                        if zero_pos in self.zero_dict:

                                            self.zero_dict[zero_pos].config(bg="blue")
                                            zero_nexttoeach.append(position)



                    print(set(zero_nexttoeach)),"comp",set(old_list)
                    if set(zero_nexttoeach) == set(old_list):
                        print("breaking")
                        break
                    else:
                        print("repeat")



                    print("loop")             


                    def baum():

                        time.sleep(0.5)
                        old_zero_list = zero_nexttoeach
                        zero_nexttoeach.append(position)
                        for list_position in old_zero_list:

                            b,x = list_position.split(",")
                            for row_mod in range(-1,2,1):
                                final_row = int(b)+int(row_mod)
                                for column_mod in range(-1,2,1):
                                    finalcolumn = int(x)+int(column_mod)
                                    zero_pos = self.str_mkr(final_row,finalcolumn)
                                    if zero_pos in self.zero_dict:
                                        # das ist der punkt wo er merk das eine 0 da ist
                                        print(list_position)
                                        if zero_pos in zero_nexttoeach:
                                            print("vb")
                                            zero_nexttoeach.append(zero_pos)
                                            self.zero_dict[zero_pos].config(bg="blue")
                                            position = zero_pos                                        











            # guck nach anderen 0
            # ab jeder 0 alle 1 bis -1 buttons finden
            # config alle buttons


               

        
    def str_mkr(self,first_input,second_input):
        obj = "{},{}".format(first_input,second_input)
        return obj
    # this is funktions for right and leftclick for the mines    
    # it tracks how many mines got marked
    def action_leftright(self,minerow,minecolumn,mine_obj,State,mine):
##        print("baum",minerow,minecolumn)
##        print(mine_obj)
##        z.config(state="disabled",text="2")
##        self.label.grid(row=x, column=y)
        if State == False:        
           mine_obj.config(bg="green")
           mine_marked = "{},{}".format(minerow,minecolumn)
           self.wincounter.remove(mine)
           print(self.wincounter)
           if len(self.wincounter) == 0:
                winbox = messagebox.showinfo("Minesweeper", "YOU HAVE WON!")
           print("bomb marked")
        if State == True:
           mine_obj.config(bg="red")
           msgbox = messagebox.showinfo("Minesweeper", "Game over")
           self.reset()
    
game = Game()




