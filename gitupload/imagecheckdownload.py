import os
import time

def scan_folder():
    imagelocation = "/home/pi/red/advent/camera_projects/adventimages"
    soundSuffix =".jpg"
    shortSound = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(imagelocation):
        for file in f:
            if soundSuffix in file:
                shortSound.append(os.path.join(r, file))
    return len(shortSound)
                
def fill_dir():
    os.system("scp pi2://home/pi/red/advent/camera_projects/fotos/* /home/pi/red/advent/camera_projects/adventimages")

def writedoc_tocheck():
    os.system("ssh pi2 touch /home/pi/red/advent/camera_projects/filecheck/baum.txt") 

writedoc_tocheck()
time.sleep(3)
image_len = scan_folder()
print(image_len)
fill_dir()
image_after_len = scan_folder()
print(image_after_len)
if image_len < image_after_len:
    print(image_len,"<",image_after_len)
    print("system working")
    
else:
    print("restart")
#     os.system("ssh pi2 sudo reboot")
