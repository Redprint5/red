
import sys
sys.path.append('/home/pi/red/advent/MFRC522-python')
import time
import mysql.connector as mariadb
from mfrc522 import SimpleMFRC522
import datetime
import RPi.GPIO as gpio
import os
import random
from mutagen.mp3 import MP3
self = 0
gpio.setmode(gpio.BCM)
pins_in = [23]
pins_out = [3,14,18,4,17,19,27,22,26,21]
# for left arm: out: 15, 18

gpio.setwarnings(False)
reader = SimpleMFRC522()
schoko_trig_pin = 23
burnout_timer = 10

id_list =[]
print("Hold a tag near the reader")


###
# class
####


class main_class:
    def __init__(self,pins_in,pins_out ):
        
        self.mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', database='santa')
        self.cursor = self.mariadb_connection.cursor()

        # gpio setup
        for i in pins_in:
            gpio.setup(i, gpio.IN)
        for i in pins_out:
            gpio.setup(i, gpio.OUT)
            
        # sound    
        self.shortSamplesPath = "/home/pi/red/advent/MFRC522-python/soundsamples/New2"
        self.soundSuffix = ".mp3"
        
        # timer
        self.reset_time = time.time()
        
        # NOT NEEDED!
        # light
        self.light_pin = 18
        self.light_duration = 2
        
        
        # distance
        self.echo_pin = 23
        self.trig_pin = 14
        self.distance_interval = 0.125
        print("class made")
        
        # motor 4,17,27,22
        self.mpin1 = 4
        self.mpin2 = 17
        self.mpin3 = 27
        self.mpin4 = 22
        
        self.control_pins_rev = [self.mpin4, self.mpin3, self.mpin2, self.mpin1]
        self.control_pins = [self.mpin1, self.mpin2, self.mpin3, self.mpin4]

        self.motor_alg= [
        [1,0,0,0],
        [1,1,0,0],
        [0,1,0,0],
        [0,1,1,0],
        [0,0,1,0],
        [0,0,1,1],
        [0,0,0,1],
        [1,0,0,1]
        ]

        self.motor_alg_rev = [
            [0, 0, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 1, 0],
            [0, 1, 1, 0],
            [0, 1, 0, 0],
            [1, 1, 0, 0],
            [1, 0, 0, 0],
            [1, 0, 0, 1]
        ]

        # audio

        self.soundArray = []
        # r=root, d=directories, f = files
        for r, d, f in os.walk(self.shortSamplesPath):
            for file in f:
                if self.soundSuffix in file:
                    self.soundArray.append(os.path.join(r, file))
        self.templist = self.soundArray
        #print(self.soundArray)
        
        
    # alt idea: die Dauerschleifewartet nur 9 minuten auf Eingabe, dann startet sie bluetooth_timer (oder wird unterbrochen von einer Karte

    
    
    def kill_vlc(self):
        os.system("pkill cvlc")
        os.system("pkill vlc")
    
    def play_sound(self, long=0):
        if long == 0:
            if len(self.templist) == 0:
                for sound in self.soundArray:
                    self.templist.append(sound)
            if len(self.templist) != 0:
                playSound = self.templist.pop(random.randint(0, len(self.templist)-1))
        os.system("cvlc " + playSound + " &")
          
        # with the module mutagen we get the length of a soundfile // currently working on setting the break on this time
        os.system("pkill cvlc")
        
        
    def mysql_cursor_date(self,date_input_id):

#         the_time = datetime.datetime.now()
#         the_time.strftime(format = "%d")
        self.cursor.execute("SELECT zeitstempel FROM date_id WHERE card_id={}".format(date_input_id))
        templist = []
        for i in self.cursor:
           templist.append(i)
           print("result1: ", templist)
           return templist
    
    
    # übergibt den Tag - "13"
    def mysql_date_timeupdate(self):


        self.cursor.execute("SELECT now()")
        for i in self.cursor:
            tempdatenow = i
            for tempdatenow2 in tempdatenow:
                tempdatenow3 = tempdatenow2
                str_id = str(tempdatenow3)
                sliced_id = str_id[8:-9]
        print("mysql_date_timeupdate_requested: ",sliced_id)
        return int(sliced_id)

        self.mariadb_connection.commit()
        self.mariadb_connection.rollback()
        self.mariadb_connection.close()

    def mysql_inj(self,number):

        x = "({})".format(number)
        self.cursor.execute("INSERT INTO `cards` (card) VALUES {}".format(x))

        self.mariadb_connection.commit()
        self.mariadb_connection.rollback()
        self.mariadb_connection.close()
        
    def mysql_read(self):

        self.cursor.execute("SELECT card FROM cards WHERE id>0")

        for i in self.cursor:
            print("card: {}".format(i))

        self.mariadb_connection.commit()
        self.mariadb_connection.rollback()
        self.mariadb_connection.close()
    
    def mysql_cursor(self):

        self.cursor.execute("SELECT card FROM cards WHERE id>0")
        templist = []
        for i in self.cursor:
            templist.append(i)

        return templist

        self.mariadb_connection.commit()
        self.mariadb_connection.rollback()
        self.mariadb_connection.close()
    
    
    
    def mysql_request(self, card_request):

        self.cursor.execute("SELECT id FROM `cards` WHERE card={}".format(card_request))

        for i in self.cursor:
            baum =i[0]
            print(baum)
            return("{}".format(baum))
                
        self.mariadb_connection.commit()
        self.mariadb_connection.rollback()
        self.mariadb_connection.close() 
        

        
    def mysql_datetime_update(self, card_request):

        self.cursor.execute("UPDATE `date_id` SET `zeitstempel` = UNIX_TIMESTAMP('0000-00-00 00:00:00') WHERE `date_id`.`card_id` = {};".format(card_request))

        for i in self.cursor:
            baum =i[0]
            print(baum)
            return("{}".format(baum))
                
                                                   

        
    def mysql_25_static(self):

        self.cursor.execute("UPDATE `date_id` SET `zeitstempel` = UNIX_TIMESTAMP('2019-11-30 12:12:12') WHERE `date_id`.`card_id` = 25;")

        self.mariadb_connection.commit()
        self.mariadb_connection.rollback()
        self.mariadb_connection.close()
        
    def mysql_connect(self, query):


        self.cursor.execute(query);
        self.mariadb_connection.commit()
        self.mariadb_connection.rollback()
        self.mariadb_connection.close()
        return self.cursor
    def mysql_specialcards(self):
        
        self.mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', database='santa')
        self.cursor = self.mariadb_connection.cursor(buffered=True)
        
        self.cursor.execute("SELECT cards_id,bezeichnung,card FROM specialcards,cards WHERE cards.id = specialcards.cards_id AND active=1;")

        sp_list = []
        for i in self.cursor:
            print(i,"this is special we test now")
            sp_list.append(str(i[2]))
        return sp_list
        
    def mysql_log(self,topic,description,level):
        ## defining the Query
        query = "INSERT INTO logs (topic,description,level) VALUES (%s, %s, %s)"
        ## storing values in a variable
        values = (topic,description,level)
        ## executing the query with values
        self.cursor.execute(query, values)
        
#        x = "('{}','{}',{})".format(topic,description,level)
#        self.cursor.execute("INSERT INTO logs (topic,description,level) VALUES {}".format(x))
        self.mariadb_connection.commit()
#        self.mariadb_connection.rollback()
#        self.mariadb_connection.close()
    def trig(self):
        shoko_input = gpio.input(schoko_trig_pin)
        if shoko_input == 1:
            return True
        else:
            return False
        
    def Nadelsensor():
        #schoko_trig_pin = 23
        burnout_start_time = time.time()

        #gpio.setup(schoko_trig_pin, gpio.IN)
            
        print("start needle")
        ################################################
        #Y'
        y = 0
        for i in range(7):
    # no contact for 7 times(trig()) activates the z                 
            if self.trig():
                y+=1
                print("das ist y",y)
            else:
                print("no trig from needle contact somehow")


            
            if y >= 5:
                while True:
                    #Z#
                    z=0
                    if self.trig()==False:
                        for i in range(7):
                            
                            if self.trig()==False:
                                z+=1
                                print("das ist z",z)
                            else:
                                print("Z lost")
                                
                                
                                
                    if z >= 5:
                        return y


        
        #################################################
        while True:
#             if burnout_start_time+burnout_timer <= time.time():
#                 break
            x=0
            if self.trig():
                for i in range(7):
                    
                    if self.trig():
                        x+=1
                        print("das ist x",x)
                    else:
                        x=0
            if x >= 3:
                break


    def rightarmimport(self):
        servoPIN = 21
        relayPIN =  27

        p = gpio.PWM(servoPIN, 50) # servoPIN for PWM with 50Hz
        p.start(2.5) # Initialization

        #schoko_trig_pin = 23

        TRIG = 16
        ECHO = 20
        relayPINf =  22
        trig_interval = 0.05


        time_sleep = 0.1
        
        
        



        def turn(turn_set, time_set):
            p.ChangeDutyCycle(turn_set)
            time.sleep(time_set)
            
        time_slow = 0.5

        gpio.output(relayPIN, gpio.HIGH)
        for i in range(9,2,-1):
            turn(i,0.5)
            print(i)
            
            
        gpio.output(relayPIN,gpio.LOW)         
        gpio.output(relayPINf, gpio.HIGH)
        print(self.Nadelsensor(),"this is nadelsensor success")
        gpio.output(relayPINf, gpio.LOW)
     




        gpio.output(relayPIN,gpio.HIGH)
        time.sleep(1)
        for i in range(3,10,1):
            turn(i,0.5)
            print(i)
        gpio.output(relayPIN,gpio.LOW)
        time.sleep(1)


##########
######################################################################################   
# class end#   
################################################################################################        






# creates a file in a specific folder
def writedoc_tocheck():
    os.system("ssh pi2 touch /home/pi/red/advent/camera_projects/filecheck/baum.txt")
    
    
# Trigger for the needele     
def trig():
    shoko_input = gpio.input(schoko_trig_pin)
    if shoko_input == 1:
        return True
    else:
        return False
    
def Nadelsensor():
    #schoko_trig_pin = 23
    burnout_start_time = time.time()

    #gpio.setup(schoko_trig_pin, gpio.IN)
        
    print("start needle")
    ################################################
    #Y'
    y = 0
    for i in range(7):
# no contact for 7 times(trig()) activates the z                 
        if trig():
            y+=1
            print("das ist y",y)
        else:
            print("no trig from needle contact somehow")


        
        if y >= 5:
            while True:
                #Z#
                z=0
                if trig()==False:
                    for i in range(7):
                        
                        if trig()==False:
                            z+=1
                            print("das ist z",z)
                        else:
                            print("Z lost")
                            
                            
                            
                if z >= 5:
                    return y


    
    #################################################
    while True:
        if burnout_start_time+burnout_timer <= time.time():
            break
        x=0
        if trig():
            for i in range(7):
                
                if trig():
                    x+=1
                    print("das ist x",x)
                else:
                    x=0
        if x >= 3:
            break


def rightarmimport():
    servoPIN = 21
    relayPIN =  27

    p = gpio.PWM(servoPIN, 50) # servoPIN for PWM with 50Hz
    p.start(2.5) # Initialization

    #schoko_trig_pin = 23

    TRIG = 16
    ECHO = 20
    relayPINf =  22
    trig_interval = 0.05


    time_sleep = 0.1
    
    
    



    def turn(turn_set, time_set):
        p.ChangeDutyCycle(turn_set)
        time.sleep(time_set)
        
    time_slow = 0.5

    gpio.output(relayPIN, gpio.HIGH)
    for i in range(9,2,-1):
        turn(i,0.5)
        print(i)
        
        
    gpio.output(relayPIN,gpio.LOW)         
    gpio.output(relayPINf, gpio.HIGH)
    print(Nadelsensor(),"this is nadelsensor success")
    gpio.output(relayPINf, gpio.LOW)
 




    gpio.output(relayPIN,gpio.HIGH)
    time.sleep(1)
    for i in range(3,10,1):
        turn(i,0.5)
        print(i)
    gpio.output(relayPIN,gpio.LOW)
    time.sleep(1)
    
def right_clean():
    
    relayPINf =  22
    gpio.setup(relayPINf, gpio.OUT)
    gpio.output(relayPINf, gpio.HIGH)
    

            

    def turn(turn_set, time_set):
        p.ChangeDutyCycle(turn_set)
        time.sleep(time_set)

        

    gpio.output(relayPIN, gpio.HIGH)
    for i in range(10,5,-1):
        turn(i,0.5)
        print(i)
    time.sleep(1)
    os.system("ssh pi2 touch /home/pi/red/advent/camera_projects/filecheck/baum.txt")
    time.sleep(1)
    for i in range(6,10,1):
        turn(i,0.5)
        print(i)

    time.sleep(1)
    gpio.output(relayPIN, gpio.HIGH)
    gpio.output(relayPIN, gpio.LOW)  
    
        
# main
# db=Connection()
Baumhaus = main_class(pins_in,pins_out)
special_cards = Baumhaus.mysql_specialcards()

list_for_cards = Baumhaus.mysql_cursor()
# Baumhaus.mysql_log(topic,description,level)
Baumhaus.mysql_log("boot","start of the programm(the very start)",10)



try:
    while True:
            print("now scanning")
            

            id, text = reader.read()
            print(id)
            ID = str(id)
            print("inputid: "+ID)
            Baumhaus.mysql_log("RIFD card scanned","this got scanned:  {}".format(ID),10)
            
            
            # custom IDs
            if ID == "665243293179":
                #leftarmimport()
                print("left arm - out of service")
                
            

            datenow = Baumhaus.mysql_date_timeupdate()
            print(datenow)
            for i in list_for_cards:
                str_id = str(i)
                sliced_id = str_id[1:-2]
                if sliced_id == ID:
                    result_search = sliced_id
                    mysql_searched = Baumhaus.mysql_request(ID)
      
                    print("idinput {}   ".format(ID),"idfound: {} ".format(result_search))
                    print("mysql searched  {}".format(mysql_searched))
                    Baumhaus.mysql_log("ID scanned:", "this ID got scanned: {}".format(mysql_searched),12)
                    #INSERT INTO `cards` (card) VALUES {}
                    
                    position = "date_id"

                    print(Baumhaus.mysql_date_timeupdate())

                    date_list = Baumhaus.mysql_cursor_date(mysql_searched)
                    for date_obj in date_list:
                        for i in date_obj:
                            str_date = str(i)
                            sliced_date = (str_date[8:-9])
                            print("comparison: ",int(sliced_date),(Baumhaus.mysql_date_timeupdate()))
                            print("das ist special:","  ", "conaprison", ID)
                            if ID in special_cards:
                                Baumhaus.mysql_log("Comparison success","special card used",11)
                                print("das ist special:","  ", "conaprison", ID)
                                Baumhaus.kill_vlc()
                                Baumhaus.play_sound()
                                Baumhaus.rightarmimport()
                                Baumhaus.mysql_log("Schoki out(special)", "schoki was sucessfully deployed special", 15)
    #                             Baumhaus.mysql_datetime_update(mysql_searched)
                                
                            elif int(sliced_date) <= (Baumhaus.mysql_date_timeupdate()-1) or sliced_date == "30":
                                print("true")
                                Baumhaus.mysql_datetime_update(mysql_searched)
                                Baumhaus.kill_vlc()
                                Baumhaus.play_sound()
                                #writedoc_tocheck()
                                rightarmimport()
                                Baumhaus.mysql_log("schoki out(normal)", "Schoki was successfully deploayed normal", 15) 
                                #writedoc_tocheck()

                            else:
                                print("you already had candy - cardid: {}".format(mysql_searched))
                                Baumhaus.mysql_log("Second use this day of card","This ID got used twice {}".format(mysql_searched),17)
            time.sleep(2)
            print("go")
except:
    ourtime = time.strftime("%d %H %M ")
    error_doc = open("error_doc.txt", "w")
    error_doc.writelines("programm crashed at: "+ ourtime+"\n")
    error_doc.writelines("^day ^hour ^minute")
    error_doc.close()