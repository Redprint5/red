import RPi.GPIO as GPIO
import sys
sys.path.append('/home/pi/red/advent/MFRC522-python')
import time
import mysql.connector as mariadb
from mfrc522 import SimpleMFRC522
import datetime
import RPi.GPIO as gpio
import os
import random
from mutagen.mp3 import MP3
gpio.setmode(gpio.BCM)
pins_in = [23]
pins_out = [14, 18,4,17,27,22]


reader = SimpleMFRC522()

id_list =[]
print("Hold a tag near the reader")


# def add_console():
#     def writedoc(read_i):
#             iddoc = open("iddoc.txt", "w")
#             iddoc.writelines(read_i)
#             iddoc.close()
# 
#     while True:
#             id, text = reader.read()
#             print(id)
#             ID = str(id)
# 
# 
#             id_list.append((ID+" "))
#             print(id_list)
#             print(len(id_list))
#             writedoc(id_list)
#             print(text)
#             time.sleep(2)
#             print("go")



class Haus:
    def __init__(self, pins_in, pins_out):
        # gpio setup
        for i in pins_in:
            gpio.setup(i, gpio.IN)
        for i in pins_out:
            gpio.setup(i, gpio.OUT)
        self.shortSamplesPath = "/home/pi/red/advent/MFRC522-python/soundsamples/New2"
        self.soundSuffix = ".mp3"
        
        
        
        # light
        self.light_pin = 18
        self.light_duration = 2
        
        
        # distance
        self.echo_pin = 23
        self.trig_pin = 14
        self.distance_interval = 0.125
        print("class made")
        
        # motor 4,17,27,22
        self.mpin1 = 4
        self.mpin2 = 17
        self.mpin3 = 27
        self.mpin4 = 22
        
        self.control_pins_rev = [self.mpin4, self.mpin3, self.mpin2, self.mpin1]
        self.control_pins = [self.mpin1, self.mpin2, self.mpin3, self.mpin4]

        self.motor_alg= [
        [1,0,0,0],
        [1,1,0,0],
        [0,1,0,0],
        [0,1,1,0],
        [0,0,1,0],
        [0,0,1,1],
        [0,0,0,1],
        [1,0,0,1]
        ]

        self.motor_alg_rev = [
            [0, 0, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 1, 0],
            [0, 1, 1, 0],
            [0, 1, 0, 0],
            [1, 1, 0, 0],
            [1, 0, 0, 0],
            [1, 0, 0, 1]
        ]

        # audio

        self.soundArray = []
        # r=root, d=directories, f = files
        for r, d, f in os.walk(self.shortSamplesPath):
            for file in f:
                if self.soundSuffix in file:
                    self.soundArray.append(os.path.join(r, file))
        print(self.soundArray)

    def light(self):
        gpio.output(self.light_pin, gpio.HIGH)
        time.sleep(self.light_duration)
        gpio.output(self.light_pin, gpio.LOW)
        
    def distance(self):
        time.sleep(self.distance_interval)

        gpio.output(self.trig_pin, True)
        time.sleep(0.0001)
        gpio.output(self.trig_pin, False)

        while gpio.input(self.echo_pin) == False:
            start = time.time()
        while gpio.input(self.echo_pin) == True:
            end = time.time()

        traveltime = end-start

        distance = traveltime / 0.000058

        print("Distance {}".format(distance))
        return(distance)
    
    
    def play_sound(self, long=0):

        if long == 0:
            if len(self.soundArray) == 0:
                for sound in sounds:
                    self.soundArray.append(sound)
            if len(self.soundArray) != 0:
                playSound = self.soundArray.pop(random.randint(0, len(self.soundArray)-1))
        os.system("cvlc " + playSound + " &")
        
        # audio = MP3(playSound)
        # select the soundfile as audio
        # len_audio = audio.info.length
        
        # with the module mutagen we get the length of a soundfile // currently working on setting the break on this time
        os.system("pkill cvlc")
        
    def motor(self, status):
        if status == "turn":
            print("normal turn")
            for i in range(512):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins[pin], self.motor_alg[halfstep][pin])
                    time.sleep(0.001)
        if status == "reverse":
            print("reverse turn")
            for i in range(512):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins_rev[pin], self.motor_alg_rev[halfstep][pin])
                    time.sleep(0.001)

        if status == "half":
            for i in range(256):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins_rev[pin], self.motor_alg[halfstep][pin])
                    time.sleep(0.001)


        if status == "quarter":
            for i in range(128):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins_rev[pin], self.motor_alg[halfstep][pin])
                    time.sleep(0.001)











def mysql_cursor_date(date_input_id):
    mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', 
    database='santa')
    cursor = mariadb_connection.cursor()


    cursor.execute("SELECT zeitstempel FROM date_id WHERE card_id={}".format(date_input_id))
    templist = []
    for i in cursor:
       templist.append(i)
       print("result1: ", templist)
       return templist
    

    mariadb_connection.commit()
    mariadb_connection.rollback()
    mariadb_connection.close() 
              

def mysql_date_timeupdate():
    
    mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', 
    database='santa')
    cursor = mariadb_connection.cursor()


    cursor.execute("SELECT now()")
    for i in cursor:
        tempdatenow = i
        for tempdatenow2 in tempdatenow:
            tempdatenow3 = tempdatenow2
            str_id = str(tempdatenow3)
            sliced_id = str_id[8:-9]
    print("mysql_date_timeupdate_requested: ",sliced_id)
    return int(sliced_id)

    mariadb_connection.commit()
    mariadb_connection.rollback()
    mariadb_connection.close()





def mysql_inj(number):
    mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', 
    database='santa')
    cursor = mariadb_connection.cursor()

    x = "({})".format(number)
    cursor.execute("INSERT INTO `cards` (card) VALUES {}".format(x))


    mariadb_connection.commit()
    mariadb_connection.rollback()
    mariadb_connection.close() 
    
def mysql_read():
    mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', 
    database='santa')
    cursor = mariadb_connection.cursor()


    cursor.execute("SELECT card FROM cards WHERE id>0")

    for i in cursor:
        print("card: {}".format(i))
        


    mariadb_connection.commit()
    mariadb_connection.rollback()
    mariadb_connection.close() 
    
def mysql_cursor():
    mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', 
    database='santa')
    cursor = mariadb_connection.cursor()


    cursor.execute("SELECT card FROM cards WHERE id>0")
    templist = []
    for i in cursor:
        templist.append(i)

    return templist


    mariadb_connection.commit()
    mariadb_connection.rollback()
    mariadb_connection.close()
    

    
def mysql_request(card_request):
    mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', database='santa')



    cursor = mariadb_connection.cursor()


    cursor.execute("SELECT id FROM `cards` WHERE card={}".format(card_request))

    for i in cursor:
        baum =i[0]
        print(baum)
        return("{}".format(baum))
            


    mariadb_connection.commit()
    mariadb_connection.rollback()
    mariadb_connection.close() 
        
def mysql_datetime_update(card_request):
    mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', database='santa')



    cursor = mariadb_connection.cursor()


    cursor.execute("UPDATE `date_id` SET `zeitstempel` = UNIX_TIMESTAMP('0000-00-00 00:00:00') WHERE `date_id`.`card_id` = {};".format(card_request))

    for i in cursor:
        baum =i[0]
        print(baum)
        return("{}".format(baum))
            


    mariadb_connection.commit()
    mariadb_connection.rollback()
    mariadb_connection.close()
    
def mysql_25_static():
    mariadb_connection = mariadb.connect(user='blue', password='a1nb3l', database='santa')



    cursor = mariadb_connection.cursor()


    cursor.execute("UPDATE `date_id` SET `zeitstempel` = UNIX_TIMESTAMP('2019-11-30 12:12:12') WHERE `date_id`.`card_id` = 25;")




    mariadb_connection.commit()
    mariadb_connection.rollback()
    mariadb_connection.close()
    
def writedoc_tocheck():
    baumdoc_check = open("checkdoc.txt", "w")
    baumlines = baumdoc_check.writelines("1")
    baumdoc_check.close()
    

        
           
    
    
# this serves to input to mysql the document you have created with python2docmaker.py

# while True:
#     check_input = input("action: ")
#     if check_input == "add":
#         id_input = input("ID: ")
#         ID = id_input
# 
#         id_list.append((ID+" "))
#         print(id_list)
#         print(text)
#     if check_input == "load":
#         doctype = input("wich doc?: ")
#         docopen = open(doctype,"r")
#         id_list = docopen.readline().split()
#         docopen.close()
# 
#     if check_input == "save":
#         
#         for i in id_list:
#             mysql_inj(i)
#     if check_input == "studown":
#         break
#     if check_input == "read":
#         print(id_list)
#         mysql_read()

    

        
Baumhaus = Haus(pins_in,pins_out)   
        
# main
while True:
        id, text = reader.read()
        print(id)
        ID = str(id)
        print("inputid: "+ID)
        list_for_cards = mysql_cursor()
        datenow = mysql_date_timeupdate()
        print(datenow)
        for i in list_for_cards:
            str_id = str(i)
            sliced_id = str_id[1:-2]
            print(sliced_id)
            if sliced_id == ID:
                result_search = sliced_id
                mysql_searched = mysql_request(ID)
  
                print("idinput {}   ".format(ID),"idfound: {} ".format(result_search))
                print("mysql searched  {}".format(mysql_searched))
                #INSERT INTO `cards` (card) VALUES {}

                
                position = "date_id"

                print(mysql_date_timeupdate())

                date_list = mysql_cursor_date(mysql_searched)
                for date_obj in date_list:
                    for i in date_obj:
                        str_date = str(i)
                        sliced_date = (str_date[8:-9])
                        print("comparison: ",int(sliced_date),(mysql_date_timeupdate()))
                        if int(sliced_date) <= (mysql_date_timeupdate()-1) or sliced_date == "30":
                            print("true")
                            mysql_datetime_update(mysql_searched)
                            mysql_25_static()
                            #writedoc_tocheck()
                            #Baumhaus.play_sound()
                            #Baumhaus.motor("turn")
                        else:
                            print("you already had candy")
                            mysql_25_static()
                
                break
            
        time.sleep(2)
        print("go")
        

            
                
        

        
        