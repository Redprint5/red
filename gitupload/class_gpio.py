import RPi.GPIO as gpio
import time
from mfrc522 import SimpleMFRC522
import sys
gpio.setmode(gpio.BCM)
pins_in = [23]
pins_out = [14, 18]


sys.path.append('/home/pi/MFRC522-python')




class Haus:
    def __init__(self, pins_in, pins_out):
        # gpio setup
        for i in pins_in:
            gpio.setup(i, gpio.IN)
        for i in pins_out:
            gpio.setup(i, gpio.OUT)
        
        
        
        # light
        self.light_pin = 18
        self.light_duration = 2
        
        
        # distance
        self.echo_pin = 23
        self.trig_pin = 14
        self.distance_interval = 0.125
        print("class made")
        
        # motor
        self.mpin1 = 14
        self.mpin2 = 15
        self.mpin3 = 18
        self.mpin4 = 23
        
        self.control_pins_rev = [self.mpin4, self.mpin3, self.mpin2, self.mpin1]
        self.control_pins = [self.mpin1, self.mpin2, self.mpin3, self.mpin4]

        self.motor_alg= [
        [1,0,0,0],
        [1,1,0,0],
        [0,1,0,0],
        [0,1,1,0],
        [0,0,1,0],
        [0,0,1,1],
        [0,0,0,1],
        [1,0,0,1]
        ]

        self.motor_alg_rev = [
            [0, 0, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 1, 0],
            [0, 1, 1, 0],
            [0, 1, 0, 0],
            [1, 1, 0, 0],
            [1, 0, 0, 0],
            [1, 0, 0, 1]
        ]

        # audio

        self.soundArray = []
        # r=root, d=directories, f = files
        for r, d, f in os.walk(shortSamplesPath):
            for file in f:
                if soundSuffix in file:
                    self.soundArray.append(os.path.join(r, file))
        print(self.soundArray)

    def light(self):
        gpio.output(self.light_pin, gpio.HIGH)
        time.sleep(self.light_duration)
        gpio.output(self.light_pin, gpio.LOW)
        
    def distance(self):
        time.sleep(self.distance_interval)

        gpio.output(self.trig_pin, True)
        time.sleep(0.0001)
        gpio.output(self.trig_pin, False)

        while gpio.input(self.echo_pin) == False:
            start = time.time()
        while gpio.input(self.echo_pin) == True:
            end = time.time()

        traveltime = end-start

        distance = traveltime / 0.000058

        print("Distance {}".format(distance))
        return(distance)
    
    
    def play_sound(self, long=0):

        if long == 0:
            if len(self.soundArray) == 0:
                for sound in sounds:
                    self.soundArray.append(sound)
            if len(self.soundArray) != 0:
                playSound = self.soundArray.pop(random.randint(0, len(self.soundArray)-1))
        os.system("cvlc " + playSound + " &")
        
        audio = MP3(playSound)
        # select the soundfile as audio
        len_audio = audio.info.length
        # with the module mutagen we get the length of a soundfile // currently working on setting the break on this time
        os.system("pkill cvlc")
        
    def motor(self, status):
        if status == "turn":
            print("normal turn")
            for i in range(512):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins[pin], self.motor_alg[halfstep][pin])
                    time.sleep(0.001)
        if status == "reverse":
            print("reverse turn")
            for i in range(512):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins_rev[pin], self.motor_alg_rev[halfstep][pin])
                    time.sleep(0.001)

        if status == "half":
            for i in range(256):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins_rev[pin], self.motor_alg[halfstep][pin])
                    time.sleep(0.001)


        if status == "quarter":
            for i in range(128):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins_rev[pin], self.motor_alg[halfstep][pin])
                    time.sleep(0.001)

        else:
            print("turn {} units".format(status))
            for i in range(status):
                for halfstep in range(8):
                    for pin in range(4):
                        gpio.output(self.control_pins_rev[pin], self.motor_alg[halfstep][pin])
                    time.sleep(0.001)




                  
# Main
x = 0
while True:
    try:
        baumdoc = open("haus.txt","r")
        baumdoclines = baumdoc.readlines()
        x+=1
        print(x)
        baumdoc.close()
    except:
        continue 


gpio.cleanup()
                    
        
        