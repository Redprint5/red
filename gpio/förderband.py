import RPi.GPIO as gpio
import time
gpio.setmode(gpio.BCM)
#
##

# init



TRIG = 16
ECHO = 20
relayPIN =  22
trig_interval = 0.5

gpio.setup(TRIG, gpio.OUT)
gpio.setup(ECHO, gpio.IN)
def seven_and_zero(x):
    if x < 7:
        if x > 0:
            return True

gpio.setup(relayPIN, gpio.OUT)
gpio.output(relayPIN, gpio.HIGH)



##

#main

#

while True:
    gpio.output(TRIG, True)
    time.sleep(0.0001)
    gpio.output(TRIG, False)

    while gpio.input(ECHO) == False:
        start = time.time()
    while gpio.input(ECHO) == True:
        end = time.time()

    sig_time = end-start
    distance = sig_time / 0.000058
    print(distance)
#     print(distance)
    if seven_and_zero(distance):
        time.sleep(trig_interval)
        print(distance)
        gpio.output(TRIG, True)
        time.sleep(0.0001)
        gpio.output(TRIG, False)

        while gpio.input(ECHO) == False:
            start = time.time()
        while gpio.input(ECHO) == True:
            end = time.time()

        sig_time = end-start
        secondtrigdistance = sig_time / 0.000058
        if seven_and_zero(secondtrigdistance):
            print(secondtrigdistance,"second trig")
            gpio.output(relayPIN,gpio.HIGH)
            break

gpio.output(relayPIN, gpio.LOW)
gpio.cleanup()
