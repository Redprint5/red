
# imports
import tkinter as tk
import os
import time
import webbrowser
import mutagen as MP3
from selenium import webdriver
import random


# init


custom_frame_size = [0.5,0.3]
system_frame_size = [0.4,0.3]



# class

class main_class:
    def __init__(self):
        
        self.shortSamplesPath = "/home/red/red/music"
        self.soundSuffix = ".mp3"
        
        
        
        self.soundArray = []
        # r=root, d=directories, f = files
        for r, d, f in os.walk(self.shortSamplesPath):
            for file in f:
                if self.soundSuffix in file:
                    self.soundArray.append(os.path.join(r, file))
        self.templist = self.soundArray
        #print(self.soundArray)
        
        
        
    def play_music(self, long=0):
        if long == 0:
            if len(self.templist) == 0:
                for sound in self.soundArray:
                    self.templist.append(sound)
            if len(self.templist) != 0:
                playSound = self.templist.pop(random.randint(0, len(self.templist)-1))
        os.system("vlc " + playSound + " &")
        audio = MP3(playsound)
        len_audio = audio.info.lenght
        time.sleep(len_audio)
          
        # with the module mutagen we get the length of a soundfile // currently working on setting the break on this time
        os.system("pkill vlc")
        
    def turnoff(self):
        os.system("sudo poweroff")
        print("poweroff")
        
        
    def keyboard(self):
        os.system("sudo matchbox-keyboard")
        print("keyboard made")
        
        
    def reboot(self):
        os.system("sudo reboot")
        print("rebooting")
        
    def gitbash(self):
        os.system("bash gitbash")
        print("gitbashed")
    
    def youtube(self):
        webbrowser.open("https://www.youtube.com")
        
    def bitbucket(self):
        webbrowser.open("https://bitbucket.org/dashboard/overview")
        
    def confirm_try(self, xpath):
        
        # this confirms the suggestions on the first row

        while True:
            try:
                
                obj = self.driver.find_element_by_xpath(xpath)
                obj.click()
                break
            except:
                continue
    
    def bvg(self):
        self.driver = webdriver.Chrome()
         # Ostendstr. (Berlin)
        # opens bvg site
        self.driver.get('https://fahrinfo.bvg.de/Fahrinfo/bin/query.bin/en')
        
        
        # selects the starting point
        start_input = self.driver.find_element_by_xpath('//*[@id="HFS_from"]')
        start_input.send_keys("Ostendstr. (Berlin)")
        self.confirm_try('//*[@id="0"]')

        
        
        # selects the destination
        destination = self.driver.find_element_by_xpath('//*[@id="HFS_to"]')
        destination.send_keys("U Siemensdamm (Berlin)") 
        self.confirm_try('//*[@id="0"]')
        
        
        
        # sets the hours
        time_hour = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        time_hour.send_keys("02")
        
        # sets the min
        time_min = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        time_min.send_keys("20")
        
        # sets the am/pm counter
        pm_set = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        pm_set.send_keys("p")
        
        
        # sets the departure to arravile time
        dep = self.driver.find_element_by_xpath('//*[@id="HFS_timesel_REQ0_0"]')
        dep.click()
        
        add_station = self.driver.find_element_by_xpath('//*[@id="queryInputButtonsFirst"]/button[2]')
        add_station.click()
        

        add = self.driver.find_element_by_xpath('//*[@id="HFS_via1"]')
        add.send_keys("S Schöneweide Bhf (Berlin)")
                
        
        
        
        # confirms the search
        confirm = self.driver.find_element_by_xpath('//*[@id="queryInputButtonsFirst"]/button[1]')
        confirm.click() 
    
  
    
# main
app = main_class()
root = tk.Tk()


# canvas is made for the frames to be placed in
canvas = tk.Canvas(root,width= 400,height= "300",bg="#263D42")
canvas.pack()




# frames are made to asssign button to the windows so they are seperate
systemframe = tk.Frame(canvas, bg="black")
systemframe.place(relwidth= system_frame_size[1], relheight= system_frame_size[0], relx= 0.05, rely=0.1)

customframe = tk.Frame(canvas, bg="black")
customframe.place(relwidth= custom_frame_size[1], relheight= custom_frame_size[0], relx= 0.55, rely=0.1)



# labels indicating the collums
systemlabel = tk.Label(canvas, bg="black", fg="green",text = "system commands")
systemlabel.place(relx=0.05, rely=0.02)


customlabel = tk.Label(canvas, bg="black", fg="green",text = "custom commands")
customlabel.place(relx=0.55, rely=0.02)

# buttons for varios actions
reboot_button = tk.Button(systemframe, padx=0.1, pady=0.2, text="reboot", bg="black",fg="white", command=app.reboot)
reboot_button.pack()

shutdown_button = tk.Button(systemframe, padx=0.1, pady=0.1, text="shutdown", bg="black",fg="white", command=app.turnoff)
shutdown_button.pack()



keyboard_button = tk.Button(customframe,padx=0.2, pady=0.2, text="keyboard", bg="black", fg="white", command=app.keyboard)
keyboard_button.pack()

gitbash_button = tk.Button(customframe,padx=0.2, pady=0.3, text="gitbash", bg="black", fg="white", command=app.gitbash)
gitbash_button.pack()

youtube_button = tk.Button(customframe,padx=0.2, pady=0.2, text="youtube", bg="black", fg="white", command=app.youtube)
youtube_button.pack()

bitbucket_button = tk.Button(customframe,padx=0.2, pady=0.2, text="bitbucket", bg="black", fg="white", command=app.bitbucket)
bitbucket_button.pack()

play_music_button = tk.Button(customframe,padx=0.2, pady=0.2, text="play_music", bg="black", fg="white", command=app.play_music)
play_music_button.pack()

searchroute = tk.Button(customframe,padx=0.2, pady=0.2, text="searchroute", bg="black", fg="white", command=app.bvg)
searchroute.pack()
