from selenium import webdriver
import time

class Bvg_bot():
    def __init__(self):
        
        # opens broswer and starts the func
        self.driver = webdriver.Chrome()
        self.bvg_open()
        
    def confirm_try(self, xpath):
        
        # this confirms the suggestions on the first row

        while True:
            try:
                
                obj = self.driver.find_element_by_xpath(xpath)
                obj.click()
                break
            except:
                continue
        
        
    def bvg_open(self):
        # Ostendstr. (Berlin)
        # opens bvg site
        self.driver.get('https://fahrinfo.bvg.de/Fahrinfo/bin/query.bin/en')
        
        
        # selects the starting point
        start_input = self.driver.find_element_by_xpath('//*[@id="HFS_from"]')
        start_input.send_keys("Ostendstr. (Berlin)")
        self.confirm_try('//*[@id="0"]')

        
        
        # selects the destination
        destination = self.driver.find_element_by_xpath('//*[@id="HFS_to"]')
        destination.send_keys("U Siemensdamm (Berlin)") 
        self.confirm_try('//*[@id="0"]')
        
        
        
        # sets the hours
        time_hour = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        time_hour.send_keys("02")
        
        # sets the min
        time_min = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        time_min.send_keys("20")
        
        # sets the am/pm counter
        pm_set = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        pm_set.send_keys("p")
        
        
        # sets the departure to arravile time
        dep = self.driver.find_element_by_xpath('//*[@id="HFS_timesel_REQ0_0"]')
        dep.click()
        
        add_station = self.driver.find_element_by_xpath('//*[@id="queryInputButtonsFirst"]/button[2]')
        add_station.click()
        

        add = self.driver.find_element_by_xpath('//*[@id="HFS_via1"]')
        add.send_keys("S Schöneweide Bhf (Berlin)")
                
        
        
        
        # confirms the search
        confirm = self.driver.find_element_by_xpath('//*[@id="queryInputButtonsFirst"]/button[1]')
        confirm.click()        
        
        
test = Bvg_bot()