from selenium import webdriver
import time

class Bvg_bot():
    def __init__(self):
        
        # opens broswer and starts the func

        self.starting = input("starting point: ")
        self.home_state = False
        if self.starting == "":
            self.starting = "Ostendstr. (Berlin)"
            self.home_state = True

        self.destination = input("destiation: ")
        if self.destination == "":
            self.destination = "U Siemensdamm (Berlin)"
            
        if self.destination == "":
            pass
        else:
            ask_time = input("time: ")
            if ask_time == "":
                ask_time = "1420"
            
            self.hour = ask_time[0:2]
            self.minute = ask_time[2:4]
            self.am_pm = "a"
            
            if int(self.hour) >= 12:
                mod_hour = str(int(self.hour) - 12)
                if len(mod_hour) == 1:
                    self.hour = "0{}".format(mod_hour)
                self.am_pm = "p"
                
            print(self.hour)
            
              
            
            
            
            
            
            
            
#             ask_time = input("time: ")
#             
#             self.split_time = int(ask_time[0:1])
#             if self.split_time <= 12:
#                 self.pm_am = "a"
#             else:
#                 self.pm_am = "p"
#             
#                 print(self.pm_am)
#                 self.split_time = int(split_time)-12
#                 print(self.split_time)
#             self.time_hn = self.split_time
#             
#             self.time_mn = ask_time[2:4]
#             print(self.time_mn)

            
        self.driver = webdriver.Chrome()
        self.bvg_open()
        
    def confirm_try(self, xpath):
        
        # this confirms the suggestions on the first row

        while True:
            try:
                
                obj = self.driver.find_element_by_xpath(xpath)
                obj.click()
                break
            except:
                continue
        
        
    def bvg_open(self):
        # Ostendstr. (Berlin)
        # opens bvg site
        self.driver.get('https://fahrinfo.bvg.de/Fahrinfo/bin/query.bin/en')
        
        
        # selects the starting point
        start_input = self.driver.find_element_by_xpath('//*[@id="HFS_from"]')
        start_input.send_keys(self.starting)
        self.confirm_try('//*[@id="0"]')

        
        
        # selects the destination
        destination = self.driver.find_element_by_xpath('//*[@id="HFS_to"]')
        destination.send_keys(self.destination) 
        self.confirm_try('//*[@id="0"]')
        
        
        


                
        time_hour = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        time_hour.send_keys(self.hour)
        
        # sets the min
        time_min = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        time_min.send_keys(self.minute)
        
        # sets the am/pm counter
        pm_set = self.driver.find_element_by_xpath('//*[@id="HFS_time_REQ0"]')
        pm_set.send_keys(self.am_pm)
            
                

         
        
        # sets the departure to arravile time
        dep = self.driver.find_element_by_xpath('//*[@id="HFS_timesel_REQ0_0"]')
        dep.click()
        
        # if home is choosen then schöneweide is taken as middle point
        if self.home_state:
            # chooses the add station
            add_station = self.driver.find_element_by_xpath('//*[@id="queryInputButtonsFirst"]/button[2]')
            add_station.click()
            # inputs schöneweide
            add = self.driver.find_element_by_xpath('//*[@id="HFS_via1"]')
            add.send_keys("S Schöneweide Bhf (Berlin)")
                
        
        
        
        # confirms the search
        confirm = self.driver.find_element_by_xpath('//*[@id="queryInputButtonsFirst"]/button[1]')
        confirm.click()        
        
        
test = Bvg_bot()