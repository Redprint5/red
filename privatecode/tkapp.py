import tkinter as tk
from tkinter import filedialog, Text
import os

col_app="red"
apps = []
if os.path.isfile("save.txt"):
    with open("save.txt","r") as f:
        tempApps = f.read()
        tempApps = tempApps.split(",")
        apps = [x for x in tempApps if x.strip()]


def addApp():

    for widget in frame.winfo_children():
        widget.destroy()


    filename = filedialog.askopenfilename(initialdir="/", title="select File",
                                          filetypes=(("executables","*.exe"),("all files", "*.*")))
    apps.append(filename)
    print(filename)

    for app in apps:
        label = tk.Label(frame, text=app, bg="grey")
        label.pack()
    col_app="green"

def runApps():
    for app in apps:
        os.startfile(app)

def deleteselect():
    for widget in frame.winfo_children():
        widget.destroy()
        
    apps.clear()



root = tk.Tk()

canvas = tk.Canvas(root, height= 700, width=700, bg="#263D42")
canvas.pack()
# Background is made by canvas command and deployed by pack() cmd

frame = tk.Frame(root, bg="white")
frame.place(relwidth= 0.8, relheight= 0.6, relx= 0.1, rely=0.1)
# this is the frame inside(white)


openFile = tk.Button(root, text="Open file", padx=10, pady=5, fg="white", bg="#263D42", command= addApp)
openFile.pack()

runApps = tk.Button(root, text="Run apps", padx=10, pady=5, fg=col_app, bg="#263D42", command=runApps)
runApps.pack()

deleteApps = tk.Button(root, text="delete apps", padx=10, pady=5, fg="white",bg="#263D42", command=deleteselect)
deleteApps.pack()

for app in apps:
    label = tk.Label(frame, text=app, bg = "grey")
    label.pack()

root.mainloop()

with open("save.txt", "w") as f:
    for app in apps:
        f.write(app + ",")

