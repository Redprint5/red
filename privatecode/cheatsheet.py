import os


# greeting = "Hello"
# name = "Michal"



# message = f"{greeting}, {name.upper()}. Welcome!" #f string with placeholder you can fill with string and code to it



# print(help(str.split)) # asks what the cmd does



# num = 3.13
# print(type(num))  # checks if float or int



# print(3 // 2) # floor
# print(3 * (2 + 1))



# num = 1
# num *= 2
# print(num) # = 2



# (abs(-3)) # = +3
# print(round(3.75,1)) # rounds ,1 indicates which 0 to start with



# num_1 = 3
# num_2 = 2
# print(num_1 >= num_2)


# num_1 = "100"
# num_2 = "200"

# num_1 = int(num_1)
# num_2 = int(num_2)

# print(num_1 + num_2)  # casts code into int etc.
# from builtins import print

# nums = [1,2,3,4,5]
# courses = ["Math","Sport","Physics","Compsci"]
# courses_2 = ["cyka","blyat"]

# courses.append("art")
# courses.insert(0,courses_2)
# courses.remove(courses[])
# popped = courses.pop()
# print(popped)
# courses.reverse()
# courses.sort(reverse=True)
# scourses = sorted(courses)
#  print(scourses)


# print(courses.index("Compsci"))
# print("Art" in courses)
# for index, courses in enumerate(courses, start=1):
#    print(courses)

# courses_str = " + ".join(courses)

# new_list = courses_str.split

# new_list = courses_str.split(" , ")

# print(courses_str)

# print(courses)
# print(len(courses[2]))
# print(courses[2:])


# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#            DICTIONARIES
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
from locale import str

'''

 student = {"name": "john", "age": "25", "othercourses": ["math","baum"],1:"deine muda"}
 print(student["name"]+student[1])
'''

# this is a dictionary you can assign values with : to other ones john is string age is int courses is an array you can request them individually

# student["phone"] = "555-5555"
# ["name"] = "jane"
# updates the dic with the values always look for the order from top to down

# student.update({"name": "maria", "age": 26, "phone": "555-5432" })
# updates the dic as it would be a dic(written) you can update multiple things and faster than plus

# del student["age"]
# del from student


# print(student.values())
# returns only the assigned number or strings etc.

# print(len(student))
# prints lenght as usual


# print(student.get("phone", "not found"))
# is the same but return none instead of an error. So if you have an input assigned then rather use this// adding ,"text" will output the text if no result is positive

# dict={"Baum":2, "Haus":3, "Raus":4}
# 
# for key, value in dict.items():
#     print(key)
#     print(value)




# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#            IF,ELSE, ELIF
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# Comparisons:
# Equal:            ==
# Not Equal:        !=
# Greater Than:     >
# Less Than:        <
# Greater or Equal: >=
# Less or Equal:    <=
# Object Identity:  is


# == is asking for equallity between both values// if is start for asking for values
# elif is extending the search after recent check
# else is printing if no conditions are meet

# language = "python"

# if language == "python":
#    print("Language is python")
# elif language == "java":
#    print("Language is javs")


# else:
#    print("no match")

# and
# or
# not

'''
user = "admin"
logged_in = False
'''
# asks if both values are correct and then applies code underneath
'''
if user == "admin" and logged_in:
    print("adminpage")

else:
    print("bad creds")

if not (user == "admin"):
    print("you")
else:
    print("fuck")
'''
'''
a = [1,2,3]
b = a

print(id(a))
print(id(b))
print(a is b)

print(id(a) == id(b))
'''
# operator "is" is respondisble of asking for exactly the same code ID example on top only TRUE if a is b

# False Values:
    # False      ev to false
    # None       ev to false
    # 0/Zero of any numeric type      ev to false but only 0 other like 1,2,3,4,5 are true
    # Any empty sequence. For example, '', (), [].    ev to false
    # Any empty mapping. For example, {}.              ev to false
'''
condition = {}

if condition:
    print('Evaluated to True')
else:
    print('Evaluated to False')
'''

# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#            lOOPS AND iNTERACTIONS STRING
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


# these loops are like in java for starts the loop on a basis of an array [] (you can apply a scanner here?) checks for nomber with if statements // Break cuts it
'''
nums = [1 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,200]

for num in nums:
    for letter in "nope":
        print(num, letter)


    if num == 4:
        print("found jackpot ")
        continue


    if num == 8:
        print("ende ")
        break


    print(num)
'''

# goes to the number in range 1 2 3 ... range(1, 11): defines the 10
'''
for i in range(1, 11):
    print(i)
'''
'''
x = 0

while True:
    if x == 5:
        break
    print(x)
    x += 1

# while loop checks for conditions iof not under goes and repeats so print first and then add 1
# condi true has to be connect with a break statement or it will be infinite
# CTRL + C breaks the programm

'''


# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                   Functions
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

'''
def hello_func(greeting, name = "You"):
    return "{} {}.".format(greeting, name)

print(hello_func("Hi", name = "Corey"))

print(len(hello_func("Hi")))

#print(hello_func())

#hello_func()
'''


# def is a method to define funktions
# pass disabled the function from before so it does not throw errors when run
# () is linking the function to the memory// without it does not work
# funk are placeholder for code or string int etc. you can set a return as an output what next to it(the string)


'''
def student_info(*args, **kwargs):
    print(args)
    print(kwargs)

courses = ["math", "Art"]
info = { "name" : "john", "age" : 22}


#student_info("math", "Art", name = "john", age = 22)

student_info(*courses, **info)
'''


# def args and kwargs is taking the input and assigning inot arays and dictionaries you can assign them by using * for array and ** for dict




'''
# Number of days per month. First value placeholder for indexing purposes.
month_days = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


def is_leap(year):
    """Return True for leap years, False for non-leap years."""

    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)


def days_in_month(year, month):
    """Return number of days in that month in that year."""

    # year 2019
    # month 9

    if not 1 <= month <= 12:
        return 'Invalid Month'

    if month == 2 and is_leap(year):
        return 29

    return month_days[month]

print(days_in_month(2019,9))
'''


# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                    File work
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#text = "sample text to save\n NEW LINE!"

#with open("example.txt", "r") as saveFile:
# contact manager is better to use
#    for line in saveFile:  # operator line is actually requesting it
#        print(line, end="") # print every line single so if big file program does not overload on memory


#saveFile = open("example.txt", "r")

# saveFile_contents = saveFile.readline() first line and if repeated it print the 2nd
# saveFile_contents = saveFile.readlines()

# print(saveFile.read()) reads the file
# print(saveFile.closed) asks if the document is closed
# print(saveFile.name) asks for the name of the file
# print(saveFile.mode) asks wich mode it is "r" "w" "a"
# saveFile.close()  closes file IMPORTAND



# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                    Class Intro
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

'''

class Employe:
    # defines a class(blueprint for creating instances)
    def __init__(self, first, last, pay):
        # assign placeholder for the class employe
        self.first = first
        # defines methods(func for class) and gives them the placeholder and a callname (only useable within class
        self.last = last
        self.pay = pay
        self.email = first + "." + last + "@conmpany.com"
        # you can use code within methods

    def fullname(self):
        return ("{} {}".format(self.first, self.last))


emp_1 = Employe("Steffan", "Sachse", 50000)
# creates class emp_1 assigns values like name etc.
emp_2 = Employe("Kevin","Fahtz", 60000)

# print(emp_1)
# print(emp_2)


print(emp_1.email)
# asks for the method email of the instance emp_1(employe1)


# print( "{} {}".format(emp_1.first, emp_1.last)) // please refer to def fullname(shortend)
# some show off code you can use doubvle {} as placeholder /// .format puts the values in this case emp1 and the first name

print(emp_1.fullname()+"@gmail.com")

emp_1.fullname()
Employe.fullname(emp_1)
# those are basicly the same in the means of you can call the class first and then give the instance or the you give the instance and it calls itself
# emp_1.fullname(self) would be the exact 

'''


# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                    Class Variables
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

'''
# class variables are shared with all instances

class Employe:

    num_of_emps = 0
    raise_amount = 1.04

    def __init__(self, first, last, pay):

        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + "." + last + "@conmpany.com"

        Employe.num_of_emps += 1
        # adds the number due to init method always being run once when a class is made

    def fullname(self):
        return ("{} {}".format(self.first, self.last))

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)
        # ises self.pay as a placeholder
        # instead of using 1.04 everytime we could just definde in the class the 1.04 and use them
        # Employe.raise_amount/self.raise:amount works too


emp_1 = Employe("Steffan", "Sachse", 50000)
emp_2 = Employe("Kevin","Fahtz", 60000)

print(emp_1.pay)
emp_1.apply_raise()
# by using the method apply_raise you change the self.pay/emp1.pay
print(emp_1.pay)

print(emp_1.__dict__)
# gives all the variables within the instance
#print(Employe.__dict__)

emp_1.raise_amount = 1.05
# changes the class spefic method. the instance gains that method from the class?
print(emp_1.__dict__)
# carefull when changing the raise_amount and the instance uses an Employe.raiseamount everything would be changed

print(Employe.raise_amount)
print(emp_1.raise_amount)
print(emp_2.raise_amount)
# if you request code from a instance but nothing is defined it will search within the class in this case raise_amount = 1.04


print(Employe.num_of_emps)
# 2 classes return is 2
'''

# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                           classmethods and staticmethods
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'''
class Employe:

    num_of_emps = 0
    raise_amount = 1.04

    def __init__(self, first, last, pay):

        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + "." + last + "@conmpany.com"

        Employe.num_of_emps += 1

        print("new class")
        print(int(Employe.num_of_emps))
        # this small code will print always when a new class is made

    def fullname(self):
        return ("{} {}".format(self.first, self.last))

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)

    @classmethod
        # this is a decorator he defines the classmethod // this changes so this can be only becalled by Employe
    def set_raise_amt(cls, ammount):
            # cls is like self it calls the the class
        cls.raise_amount = ammount
        # changes the whole class raise_amount to amount so Employe.set_raise_amt(cls, 1.05)// cls has not to be called due to Employe at the start

    @classmethod
    # this classmethod is constructor for instances from the class
    def from_string(cls, emp_str):
        first, last, pay = emp_str.split('-')
        # this is a way to have a string being converted into a new class
        # .split is usefull to remember!!!!!
        return cls(first, last, pay)


    @staticmethod
    # static methods do not use instances or classes as their values
    def is_workday(day):
        if day.weekday() == 5 or day.weekday() == 6:
            # in the module date the 0 is a monday and the 6 is the sunday(sonntag)
            return False
        return True

emp_1 = Employe("Steffan", "Sachse", 50000)
emp_2 = Employe("Kevin","Fahtz", 60000)


emp_str_1 = 'John-Doe-70000'
emp_str_2 = 'Steve-Smith-30000'
emp_str_3 = 'Jane-Doe-90000'

new_emp_1 = Employe.from_string(emp_str_1)
new_emp_2 = Employe.from_string(emp_str_2)

print(new_emp_2.email)
print(new_emp_1.pay)


#import datetime
# imports the date module
#my_date = datetime.date(2016, 7, 11)
# def date
#print(Employe.is_workday(my_date))
# print the method with the input my_date
'''

# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                           Slicing Lists and Strings
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////



my_list = [0,1,2,3,4,5,6,7,8,9]
#          0,1,2,3,4,5,6,7,8,9    placeholder number positive
#          -10-9-8-7-6-5-4-3-2,1  placeholder negative -1 end of the list


print(my_list[2:-1:2])   # 2: -1 is the range so positive 2 place to the last position due to -1 // adding [x:x:2] will step on every 2nd place
print(my_list[-1:2:-1])  # reverse
print(my_list[::-1])     # empty will tell the systme that start to end // can be used on strings aswell(quote)


sample_url = 'http://coreyms.com'
print(sample_url)

# Reverse the url
# print sample_url[::-1]

# # Get the top level domain
# print sample_url[-4:]

# # Print the url without the http://
print(sample_url[7:-2])

# # Print the url without the http:// or the top level domain
# print sample_url[7:-4]



# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                Variable Scope - Understanding the LEGB rule and global/nonlocal statements
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
'''
x = "global x"

def test(z):
    global y  #x would not work outside the function. global will put it into the global scope
    y = "local y"
    print(z)

test("local z")
# print(x)

import builtins

# print(dir(builtins)) # dir asks for all the code?

def min():   # python allows func being written again this func would not work as min(normal)
    pass

m = min([5, 1, 4, 2 ,3])  # asks for minamal value within the scope
print(m)
'''

'''
x = "global x"
#print(x)

def outer():
    x = "outer x"

    def inner():
        nonlocal x # this would move the inner x as enclosed variable
        #x = "inner x"
        print(x)  # this print x would print the comment but due to not having any other x it checks further outside in the means of x 0 "outer x" and gives that as an output

    inner()
    print(x)

outer()
print(x)

'''

# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                importing modules
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


'''
import sys
import os
import antigravity
import webbrowser

webbrowser.open("youporn.com")

print(os.getcwd())

print(antigravity.__file__)


sys.path.append("Users\Redprint5\Desktop\mymodules")

print(sys.path)
'''

# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#           import os
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import webbrowser
import os
# imports all os functions

print(os.getcwd())
from datetime import datetime



'''
# print(dir(os))
# gives all the functions and methods in this directiory in this case "os"


# print(os.getcwd())
# prints the current directory in this case desktop

# os.mkdir('New folder/su-dir-1')
# this code wont run with sub directory asking the code underneath used does it and creates a new folder(can be extended multiple times
# os.makedirs("New folder/su-dir-2")


# os.rmdir("New folder/su-dir-2")
# os.removedirs("New folder/su-dir-2")
# removes folders

# os.rename('New folder', "Demo")
# renammes the files

#print(os.stat("porndoc.txt").st_size)
# prints data from the selected doc. using st_size will return the size of the object (40 = 40 bites)

#mod_time = os.stat("porndoc.txt").st_mtime
#print(datetime.fromtimestamp(mod_time))
# st_mtime asks for lates modified time // the result is not readable normally the functions underneath helps with that


#print(os.listdir())
# prints all objects from the directory in this case out desktop


for dirpath, dirnames, filenames in os.walk(os.environ.get("USERPROFILE")):
    print("Current Path:", dirpath)
    print("Directories:", dirnames)
    print("Files:", filenames)
    print()

# searches trough all files usefull to check on forgotten files

print(os.environ.get("USERPROFILE"))


filepath = os.path.join(os.environ.get("USERPROFILE"),"test.txt")
# this joins the filepath with a document in this case test.txt you could use this method to create and delete all files whereever you like by chaning the directory
print(filepath)

# with open(filepath, "w") as f:
#    print("openened")
# this would open mentioned file


print(os.path.basename("tmp/test.txt"))
# gives the name of the file(last)
print(os.path.dirname("tmp/test.txt"))
# only directory output but not filename
print(os.path.split("tmp/test.txt"))
# gives directory and filename as output
print(os.path.exists("tmp/test.txt"))
# checks for existance
print(os.path.isdir("tmp/test.txt"))
# checks if this is a directory
print(os.path.isfile("tmp/test.txt"))
# checks fi this is a file
print(os.path.splitext("tmp/test.txt"))
# this splits the the extension
print(dir(os.path))
# this gives us a list of all results
'''


# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#           Datetime module
#           webbrowser.open("https://www.youtube.com/watch?v=eirjjyP2qcQ&list=PL-osiE80TeTt2d9bfVyTiXJA-UTHn6WwU&index=24")
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


# import datetime
# imports from the standard libary  the datetime

# import pytz
# imports the pytz package that i installed with pip manager in the cmd promt using py -m pip install pytz


'''
# naive dates and time is used for general work (easy to work with)
# print(dir(datetime))


# d = datetime.date(2016, 7, 24)
# print(d)
# defines dates and prints it do not type 07 only 7


tday = datetime.date.today()
print(tday)
print(tday.weekday())
# prints the day of the week with code either monday is 0 ^^ or monday is a 1 like down(i pref down)
print(tday.isoweekday())


# tdelta = datetime.timedelta(days=7)
# creates a timedelta like a string with the 7
# print(tday + tdelta)
# this prints the date plus the timedelta in this case +7 days as a date(can be minus aswell)

# date2 = date1 - timedelta
# timedelta = date1 - date2
# adding or subtracting dates will create a timedelta and same in reverse

# bday = datetime.date(2020, 1, 12)
# my Bday


# till_bday = bday - tday
# print(till_bday)
# print(till_bday.total_seconds())

t = datetime.time(9, 30 , 45, 10000)
# this is for time hour min sec millsec
print(t.hour)
# prints the hour

dt = datetime.datetime(2016, 7, 26, 12, 30, 45, 10000)

print(dt)
# this is best to use due to all variables being callable even with .date .time for individual usages


tdelta = datetime.timedelta(hours=13)
print(dt+tdelta)


dt_today = datetime.datetime.today()
dt_now = datetime.datetime.now()
dt_utcnow = datetime.datetime.utcnow()
# those print the current time

print(dt_today)
print(dt_now)
print(dt_utcnow)


#print(dir(pytz))

# dt = datetime.datetime(2016, 7, 27, 12, 30, 45, tzinfo=pytz.UTC)
# print(dt)
# dt_now = datetime.datetime.now(tz=pytz.UTC)
# print(dt_now)


dt_utcnow = datetime.datetime.utcnow().replace(tzinfo=pytz.UTC)
print(dt_utcnow)

dt_mtn = dt_utcnow.astimezone(pytz.timezone("US/Mountain"))
# applies the timezone "US/mountain"


# for tz in pytz.all_timezones:
#    print(tz)
# this is asking for all possible timezones you could put into the 



dt_mtn = datetime.datetime.now(tz=pytz.timezone("US/Mountain"))

print(dt_mtn.strftime("%B %d, %Y"))
# isoformat is changing the format or with strftime and you can look up the codes to change it as u like

dt_str = "July 07, 2019"

dt = datetime.datetime.strptime(dt_str, "%B %d, %Y")
print(dt)
# converts strings into datetime // strftime does the same but reverted (datetime into string)
'''

# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                                    Rasbian cmd list / linux based
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ifconfig shows ip and network of raspberry pi
# sudo raspi-config   opens the settings rasbian 
# pip install RPi.GPIO   pip install, installs GPIo scripts for gpio pins on rasberry
# cd ../   move dir and ../ is one back
# ls     list cwd or choosen one (ls /XXXX/XX/)
# mv / mvdir /XXX/XXX/baum /XXXX/XXX/haus   this moves the thing baum becomes haus
# rm / rmdir  this removes stuff
# bash   command to execute a program is in cwd
# cwd    current working directory
# alias out="cd ../"
# pip3 install XXXX    python3 module installing
# sudo apt-get install      installing programms
# ll    is like ls but in a staightdown and more info 
# pi@raspberrypi:~ $ ll /usr/bin/ |grep pkill    grep is used for searching things within directory 
# pwd      current working directory 
# pkill vlc    kills procces/ vlc for example
# crontab -e     is used for putting things into autostart/ here an autostart cmd:   
# 00      23      *       *       *       /usr/bin/cvlc /home/pi/red/advent/MFRC5$

# touch /home/pi/red/baum.txt    makes doc in dir given and name at the last



# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#                                    RPi.GPIO
# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////



# import RPi.GPIO as gpio
# 
# x = 10   # pinnumber
# 
# gpio.setmode(gpio.BCM)
# # sets all pins as bcm format? just do that once in innit
# gpio.setup(x, gpio.OUT)
# # setup individual gpio pins as output or input(gpio.IN)
# gpio.output(x, gpio.HIGH)
# # sends signal high or low(gpio,LOW)
# while gpio.input == false:
#     start
# gpio.cleanup()


# for more insights please reffer to the halloween.py or distance.py projects















