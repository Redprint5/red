import os
import RPi.GPIO as gpio
import random
import time
from mutagen.mp3 import MP3
# basic imports


 

# *****************************************
# ****************  Init  *****************
# *****************************************
pinsLed = [17,17]
pinEchoGet = 18 #ECHO
pinEchoSend = 4 #TRIG
trigerdistance = 150
spookbreak=0
scaninterval=0.01
system = True # !!!
shortSamplesPath = "/home/pi/red/soundsamples"
shortSoundArray = []
soundSuffix = ".mp3"
len_audio = ""
vlc_offset = 0.3


gpio.setmode(gpio.BCM)
for pin in pinsLed:
    gpio.setup(pin, gpio.OUT)
gpio.setup(pinEchoGet, gpio.IN)
gpio.setup(pinEchoSend, gpio.OUT)
# setup for gpio pins each led in the list will be enabled

 
shortSound = []
# r=root, d=directories, f = files
for r, d, f in os.walk(shortSamplesPath):
    for file in f:
        if soundSuffix in file:
            shortSound.append(os.path.join(r, file))
print(shortSound)
# creates the shortsound-list with all the content of the folder selected


# *****************************************
# ***********  defining the functions  ****
# *****************************************



def scan_break():
    time.sleep(scaninterval)

# scanbreak funk just waits out the set scaninterval in init
    
def list_checker(list):
    n = 0
    if list != []:    
        for i in list:
            n+=1
    return(n)
# checks the content of a list if it is empty or is has object in it. If obj are in the list they are counted            
            
            
            
def play_sound(long=0):
    # after I found out that VLC needs a proper buch of ressources - 308 extra packages with nearly 1GB - I decided to try something else: http://pyglet.org/
    # ich mußte feststellen, daß der Sch... auch nur auf pygames aufsetzt und habe dies dann (bisher erfolglos) probiert
    if long == 0:
        # im array shortSoundArray wird eine Kopie von shortSound gespeichert aus der bei jedem Aufruf ein Element entnommen wird, wenn keine Elemente mehr enthalten sind so wird das Array wieder mit shortSound gefüllt
        if len(shortSoundArray) == 0:
            for i in shortSound:
                shortSoundArray.append(i)
        if len(shortSoundArray) != 0:
        # ein zufälliges Element wird aus shortSoundArray gelöscht und in playSound zum Abspielen gespeichert
            playSound = shortSoundArray.pop(random.randint(0, list_checker(shortSoundArray)-1))
    os.system("vlc " + playSound + " &")
    
    
    
    audio = MP3(playSound)
    # select the soundfile as audio
    len_audio = audio.info.length
    # with the module mutagen we get the length of a soundfile // currently working on setting the break on this time
    return(len_audio)


    
    
    


def glow(pin_number):
    gpio.output(pin_number, gpio.HIGH)
# this function enables the light choosen for pin_number

def dis_glow(pin_number):
        gpio.output(pin_number, gpio.LOW)
# this function disables the light choosen for pin_number

# turns all lights lights_off
def lights_off():
    for led in pinsLed:
        dis_glow(led)

# schaltet bei jedem Aufruf jede LED ein oder aus (eingeschaltete LEDs bleiben beim wiederholten einschalten eingeschaltet, ausgeschaltete ebenso)
def lights_flicker():
    for led in pinsLed:
        if random.randint(0,1) == 1:
            dis_glow(led)
        else:
            glow(led)
            


def kill_vlc():
    os.system("pkill vlc")
# kills vlc proccess in bash shell    


# sollte überflüssig sein:
def lights_on(x):
        time.sleep(1)
        glow(pinsLed[1])
        time.sleep(x)
    
# ich nehme mal die Schleife raus und werfe sie in die Main:
def distance_sens():
    gpio.output(pinEchoSend, True)
    time.sleep(0.0001)
    gpio.output(pinEchoSend, False)

    while gpio.input(pinEchoGet) == False:
        start = time.time()
    while gpio.input(pinEchoGet) == True:
        end = time.time()

    sig_time = end-start
    distance = sig_time / 0.000058
    print(distance)
    return distance
# this func uses the distance sensor and returns and prints the distance


    
# *****************************************
# ****************  Main  *****************
# *****************************************
while True:
    scan_break()

    if distance_sens() <= trigerdistance:
        scan_break()
        # if the distance becomes smaller or stays the same go on.. (could be we have to rise the time)
        if distance_sens() <= trigerdistance:
            if distance_sens() <= trigerdistance:
                lights_on(play_sound()+ vlc_offset)
                kill_vlc()
                lights_off()
                time.sleep(spookbreak) # is currently 0 so ignore it
            
            # due to the playsound being in the background and py running we have to set the breaks in the lights functions
            # after lights is pased the audio will be killed and the lights shut
            



gpio.cleanup()


