import RPi.GPIO as gpio
import time


gpio.setmode(gpio.BCM)
pins_in = [23]
pins_out = [14, 18]


class Haus:
    def __init__(self, pins_in, pins_out):
        # gpio setup
        for i in pins_in:
            gpio.setup(i, gpio.IN)
        for i in pins_out:
            gpio.setup(i, gpio.OUT)
        
        
        
        # light
        self.light_pin = 18
        self.light_duration = 2
        
        
        # distance
        self.echo_pin = 23
        self.trig_pin = 14
        self.distance_interval = 0.125
        print("class made")
        
            
    def light(self):
        gpio.output(self.light_pin, gpio.HIGH)
        time.sleep(self.light_duration)
        gpio.output(self.light_pin, gpio.LOW)
        
    def distance(self):
        time.sleep(self.distance_interval)

        GPIO.output(self.trig_pin, True)
        time.sleep(0.0001)
        GPIO.output(self.trig_pin, False)

        while GPIO.input(self.echo_pin) == False:
            start = time.time()
        while GPIO.input(self.echo_pin) == True:
            end = time.time()

        traveltime = end-start

        distance = traveltime / 0.000058

        print("Distance {}".format(distance))
        return(distance)          
                  
                  
                  
# Main                  

baum = Haus(pins_in, pins_out)
baum.distance()
gpio.cleanup
                    
        
        

