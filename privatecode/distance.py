import RPi.GPIO as gpio
import time

gpio.setmode(gpio.BCM)

TRIG = 16
ECHO = 20
relayPIN = 4 # this is right

trig_interval = 0.1

gpio.setup(TRIG, gpio.OUT)
gpio.setup(ECHO, gpio.IN)



def seven_and_zero(x):
    if x < 7:
        if x > 0:
            return True

while True:
    gpio.output(TRIG, True)
    time.sleep(0.0001)
    gpio.output(TRIG, False)

    while gpio.input(ECHO) == False:
        start = time.time()
    while gpio.input(ECHO) == True:
        end = time.time()

    sig_time = end-start
    distance = sig_time / 0.000058
#     print(distance)
    print(distance)
